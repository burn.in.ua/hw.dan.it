function CreateNewUser(){

    this.firstName = prompt('What is your name?');
    this.lastName = prompt('What is your surname?');
    this.birthday = prompt('Введите дату рождения в формате дд.мм.рррр');
    this.getLogin = function() {
        let newLogin = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        return newLogin;
    }
    this.getAge = function(){
        let now = new Date().getFullYear();
        let birthdayArr = this.birthday.split('.');
        return now - Number(birthdayArr[2]);

    }
    this.getPassword = function(){
        let birthdayArr = this.birthday.split('.');
        let userPassword = this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + birthdayArr[2];
        return userPassword;
    }
    
}
let newUser = new CreateNewUser();

console.log(`Dear ${newUser.firstName}, greeting! Your age is ${newUser.getAge()}.`);
console.log(`Your login: ${newUser.getLogin()}`);
console.log(`Your password: ${newUser.getPassword()}`);