

let num1 = +prompt("Введите первое число", "");
let num2 = +prompt("Введите второе число", "");
let operation = prompt("Введите математическую операцию + - * /");
let result;

function showResult(){
    switch (operation){
        case "+": 
            result = num1 + num2;
            break;
        case "-": 
            result = num1 - num2;
            break;
        case "*": 
            result = num1 * num2;
            break;
        case "/": 
            result = num1 / num2;
            break;
    }
    return console.log(result);
}

showResult();
